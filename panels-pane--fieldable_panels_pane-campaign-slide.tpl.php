<?php
/**
 * @file Panels-pane.tpl.php
 * Main panel pane template.
 *
 * Variables available:
 * - $pane->type: the content type inside this pane
 * - $pane->subtype: The subtype, if applicable. If a view it will be the
 *   view name; if a node it will be the nid, etc.
 * - $title: The title of the content
 * - $content: The actual content
 * - $links: Any links associated with the content
 * - $more: An optional 'more' link (destination only)
 * - $admin_links: Administrative links associated with the content
 * - $feeds: Any feed icons or associated with the content
 * - $display: The complete panels display object containing all kinds of
 *   data including the contexts and all of the other panes being displayed.
 */
?>

<div class="item">
  <div class="jumbotron hero-nature carousel-hero-1">
    <h1 class="hero-title">"<?php print render($content['field_description'][0]['#markup']); ?>"</h1>
    <p class="hero-subtitle-1"><?php print render($content['field_doc_people'][0]['#markup']); ?></p>
    <p class="hero-subtitle-2"><?php print render($content['field_position'][0]['#markup']); ?></p>
  </div>
</div>  

