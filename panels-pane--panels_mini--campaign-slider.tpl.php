<div class="carousel slide" data-ride="carousel" id="carousel-1">
  <div class="carousel-inner" role="listbox">
    
   <?php print render($content); ?>

  </div>
</div>

<div class="d-none"><a class="carousel-control-prev d-none" href="#carousel-1" role="button" data-slide="prev"><i class="fa fa-chevron-left"></i><span class="sr-only">Previous</span></a><a class="carousel-control-next d-none" href="#carousel-1" role="button"
<i class="fa fa-chevron-right"></i><span class="sr-only">Next</span></a></div>
<ol class="carousel-indicators">
  <li data-target="#carousel-1" data-slide-to="0" class="active"></li>
  <li data-target="#carousel-1" data-slide-to="1"></li>
  <li data-target="#carousel-1" data-slide-to="2"></li>
</ol>
</div>

