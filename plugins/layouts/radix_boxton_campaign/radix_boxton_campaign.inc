<?php
// Plugin definition
$plugin = array(
  'title' => t('Boxton Campaign'),
  'icon' => 'radix-boxton-campaign.png',
  'category' => t('Landportal'),
  'theme' => 'radix_boxton_campaign',
  'regions' => array(
    'contentmain' => t('Content'),
  ),
);
