<?php
// Plugin definition
$plugin = array(
  'title' => t('Moscone Flipped Campaign'),
  'icon' => 'radix-moscone-flipped-campaign.png',
  'category' => t('Landportal'),
  'theme' => 'radix_moscone_flipped_campaign',
  'regions' => array(
    'header' => t('Header'),
    'sidebar' => t('Content Sidebar'),
    'contentmain' => t('Content'),
    'footer' => t('Footer'),
  ),
);
